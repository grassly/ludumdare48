﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class EnemyBase : MonoBehaviour
{
    public float maxSpeed;
    public float acceleration;
    [NonSerialized] public Transform target;
    public State state { get; protected set; }
    public float agroRadius;
    public float damage;
    public float maxHealth;
    protected float _health;
    public float health
    {
        get => _health;
        set
        {
            if (value > maxHealth)
            {
                _health = maxHealth;
            }
            else if (value <= 0)
            {
                _health = 0;
                OnDie?.Invoke();
                Die();
            }
            else
            {
                _health = value;
            }
        }
    }

    protected Rigidbody rb;
    public UnityEvent OnDie;

    bool wasScared;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public enum State
    {
        Idle,
        Agresive,
        Scary,
        Die
    }

    public virtual void SetAgresive(Transform target)
    {
        this.target = target;
        state = State.Agresive;
    }

    public virtual void SetIdle()
    {
        state = State.Idle;
    }

    public virtual void SetScary()
    {
        wasScared = true;
        state = State.Scary;
    }

    private void Start()
    {
        CreateTrigger();
        health = maxHealth;
    }

    private void Update()
    {
        EnyUpdate();
    }

    protected virtual void EnyUpdate()
    {
        if (state == State.Idle)
        {
            OnIdleUpdate();
        }
        if (state == State.Agresive)
        {
            OnAgressiveUpdate();
        }
        if (state == State.Scary)
        {
            OnScaryUpdate();
        }
        if (state != State.Die)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity), Time.deltaTime * 2);
        }
    }

    protected virtual void OnIdleUpdate()
    {
        if(wasScared && Vector3.Distance(transform.position, target.position) > 15)
        {
            Destroy(gameObject);
        }
    }

    protected virtual void OnAgressiveUpdate()
    {
    }

    protected virtual void OnScaryUpdate()
    {
    }

    private void FixedUpdate()
    {
        EnyFixedUpdate();
    }

    protected virtual void EnyFixedUpdate()
    {
        if (state == State.Idle)
        {
            OnIdleFixedUpdate();
        }
        if (state == State.Agresive)
        {
            OnAgressiveFixedUpdate();
        }
        if (state == State.Scary)
        {
            OnScaryFixedUpdate();
        }
    }

    protected virtual void OnIdleFixedUpdate()
    {

    }

    protected virtual void OnAgressiveFixedUpdate()
    {
        if (rb.velocity.magnitude < maxSpeed)
        {
            rb.AddForce((target.position - transform.position).normalized * acceleration);
        }
    }

    protected virtual void OnScaryFixedUpdate()
    {
        if (target != null && rb.velocity.magnitude < maxSpeed)
        {
            rb.AddForce(-(target.position - transform.position).normalized * acceleration);
        }
    }

    protected virtual void Die()
    {
        state = State.Die;
        StartCoroutine(ClearBody());
    }

    IEnumerator ClearBody()
    {
        yield return new WaitForSeconds(10);
        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (state == State.Idle && other.CompareTag("Player"))
        {
            SetAgresive(other.transform);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if ((state == State.Agresive || state == State.Scary) && other.CompareTag("Player"))
        {
            SetIdle();
        }
    }

    public void OnCollisionStay(Collision collision)
    {
        if (collision.collider.CompareTag("Player") && state == State.Agresive)
        {
            collision.gameObject.GetComponent<Oxygen>().oxygen -= damage * Time.deltaTime;
        }
    }

    SphereCollider CreateTrigger()
    {
        SphereCollider collider = gameObject.AddComponent<SphereCollider>();
        collider.radius = agroRadius;
        collider.isTrigger = true;
        return collider;
    }
}
