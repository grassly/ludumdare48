﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisturbingFish : EnemyBase
{


    protected override void OnScaryFixedUpdate()
    {
        if (target != null && rb.velocity.magnitude < maxSpeed * 1.5f)
        {

            rb.AddForce(Vector3.Cross((transform.position - target.position), Vector3.forward), ForceMode.Impulse);
            SetAgresive(target);
        }
    }
}
