﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazyFish : EnemyBase
{
    public float slowingDownForce = 1;

    protected override void EnyUpdate()
    {
        base.EnyUpdate();
        if (state == State.Agresive)
        {
            if (target.TryGetComponent(out Rigidbody player))
            {
                player.AddForce(-player.velocity * slowingDownForce);
            }
        }
    }
}
