﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggressiveFish : EnemyBase
{
    // Start is called before the first frame update
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && state == State.Agresive)
        {
            collision.gameObject.GetComponent<Oxygen>().oxygen -= damage;
            Destroy(gameObject);
        }
    }
}
