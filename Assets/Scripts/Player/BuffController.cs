﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffController : MonoBehaviour
{
    Oxygen oxygen;
    [SerializeField] float bonusWeight;

    void Start()
    {
        oxygen = GetComponent<Oxygen>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bonus"))
        {
            oxygen.oxygen += bonusWeight;
            Destroy(other.gameObject);
        }
    }
}
