﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Win : MonoBehaviour
{
    public float winDelay;
    public Image fadePanel;
    public int winBuldIndex;

    AudioSource audioSource;
    public float finalPhraseDelay;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void StartWin(Controller playerController)
    {
        playerController.enabled = false;
        audioSource.PlayDelayed(finalPhraseDelay);
        StartCoroutine(WinDelay());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartWin(other.GetComponent<Controller>());
        }
    }

    IEnumerator WinDelay()
    {
        float delay = winDelay;
        while (delay > 0)
        {
            yield return null;
            delay -= Time.deltaTime;
            fadePanel.color = new Color(0, 0, 0, 1 - delay / winDelay);
        }
        SceneManager.LoadScene(winBuldIndex);
    }
}
