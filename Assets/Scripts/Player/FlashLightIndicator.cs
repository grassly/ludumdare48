﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightIndicator : MonoBehaviour
{
    public Material LightOn;
    public Material LightOff;

    public MeshRenderer[] renderers;
    public LampController lamp;
    void Start()
    {
        
    }

    
    void Update()
    {
        for (int i = 0; i < renderers.Length; i++)
        {
            if(lamp.currLightEnergy / lamp.maxLightEnergy > (float)i / renderers.Length && renderers[i].material != LightOn)
            {
                renderers[i].material = LightOn;
            }
            else if(lamp.currLightEnergy / lamp.maxLightEnergy <= (float)i / renderers.Length && renderers[i].material != LightOff)
            {
                renderers[i].material = LightOff;
            }
        }
    }
}
