﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Controller : MonoBehaviour
{
    public float maxSpeed = 1;
    public float acceleration = 1;

    public float rotationSpeed = 2;
    public float rotationRatio = 5;
    
    Rigidbody rb;
    float x;
    float y;

    Quaternion startRot;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        startRot = transform.rotation;
    }

    void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(startRot.eulerAngles + new Vector3(y * rotationRatio, -x * rotationRatio, 0)), Time.deltaTime * rotationSpeed);
    }

    
    void FixedUpdate()
    {
        if (rb.velocity.magnitude < maxSpeed)
            rb.AddForce(new Vector3(x, y).normalized * acceleration);
    }
}
