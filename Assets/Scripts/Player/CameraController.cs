﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;
    public Transform target;

    void Start()
    {
        
    }

    
    void FixedUpdate()
    {
        transform.Translate((Vector2)(target.position - transform.position) * speed * Time.fixedDeltaTime);
    }
}
