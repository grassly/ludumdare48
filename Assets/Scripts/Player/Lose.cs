﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lose : MonoBehaviour
{
    public GameObject player;
    public float looseDelay;
    public Image loseFadePanel;
    public int loseBuldIndex;

    Controller playerController;
    Oxygen playerOxygen;

    private void Awake()
    {
        playerController = player.GetComponent<Controller>();
        playerOxygen = player.GetComponent<Oxygen>();
    }
    private void Start()
    {
        playerOxygen.OnOxygenLow.AddListener(StartLose);
    }
    public void StartLose()
    {
        playerOxygen.OnOxygenLow.RemoveListener(StartLose);
        playerController.enabled = false;
        StartCoroutine(LoseDelay());
    }

    IEnumerator LoseDelay()
    {
        float delay = looseDelay;
        while (delay > 0)
        {
            yield return null;
            delay -= Time.deltaTime;
            loseFadePanel.color = new Color(0, 0, 0, 1 - delay / looseDelay);
        }
        SceneManager.LoadScene(loseBuldIndex);
    }
}
