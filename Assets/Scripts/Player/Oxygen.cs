﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Oxygen : MonoBehaviour
{
    public float maxOxygen;
    float _oxygen;
    public float oxygen
    {
        get => _oxygen;
        set
        {
            if (value > maxOxygen)
            {
                _oxygen = maxOxygen;
            }
            else if(value <= 0)
            {
                _oxygen = 0;
                OnOxygenLow?.Invoke();
            }
            else
            {
                _oxygen = value;
            }
        }
    }

    public UnityEvent OnOxygenLow;

    private void Start()
    {
        oxygen = maxOxygen;
    }
}
