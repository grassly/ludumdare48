﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    AudioSource mainSource;
    Controller controller;
    Rigidbody rb;

    public AudioClip[] hitByRockSounds;
    public AudioSource hitByRockSource;

    void Start()
    {
        mainSource = GetComponent<AudioSource>();
        controller = GetComponent<Controller>();
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        mainSource.volume = rb.velocity.magnitude / controller.maxSpeed + 0.2f;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Rocks") && collision.impulse.magnitude > 1.5f)
        {
            hitByRockSource.clip = hitByRockSounds[Random.Range(0, hitByRockSounds.Length)];
            hitByRockSource.Play();
        }
    }
}
