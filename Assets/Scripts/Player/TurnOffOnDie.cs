﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffOnDie : MonoBehaviour
{
    public MeshRenderer window;
    Oxygen oxygen;
    private void Awake()
    {
        oxygen = GetComponent<Oxygen>();
        oxygen.OnOxygenLow.AddListener(TurnOff);
    }

    private void TurnOff()
    {
        window.material.SetColor("_EmissionColor", Color.black);
    }
}
