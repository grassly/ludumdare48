﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampController : MonoBehaviour
{
    public GameObject lightPoint;
    public GameObject projector;
    public float attackForce;

    [SerializeField] LayerMask enemy;

    [SerializeField] private float projectorDistance;

    public float dyingTIme;

    private bool lightOn;

    private EnemyBase enemyFish;
    public AudioSource flashlightOnSound;

    [SerializeField] private float lightDamage;
    [SerializeField] private float lightBoost;
    [HideInInspector] public float currLightEnergy;
    public float maxLightEnergy;


    void Start()
    {
        lightOn = false;
        currLightEnergy = maxLightEnergy;
    }

    public void SetLightDirection()
    {
        Vector3 position = Input.mousePosition;
        Vector3 dir = Camera.main.ScreenToWorldPoint(position + new Vector3(0,0,10));
        //Vector3 lightDirection = new Vector3(dir.x, dir.y, lightPoint.transform.position.z);
        Vector3 lightDirection = new Vector3(dir.x, dir.y, lightPoint.transform.position.z);

        Ray ray = new Ray(lightPoint.transform.position, lightDirection - lightPoint.transform.position);
        Debug.DrawRay(lightPoint.transform.position, lightDirection - lightPoint.transform.position, Color.green);

        projector.transform.LookAt(lightDirection);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, projectorDistance, enemy))
        {
            Debug.Log(hit.collider.isTrigger);
            Debug.DrawLine(ray.origin, hit.point, Color.red);
            if (!hit.collider.isTrigger)
            {
                enemyFish = hit.transform.gameObject.GetComponent<EnemyBase>();
                //StartCoroutine(KillEnemy());
                enemyFish.SetScary();
                enemyFish.health -= Time.deltaTime;
            }
        }
    }

    public IEnumerator KillEnemy()
    {
        yield return new WaitForSeconds(dyingTIme);
        enemyFish.SetScary();
    }

    public void GetDamage()
    {
        currLightEnergy -= lightDamage * Time.deltaTime;
    }

    void Update()
    {
        if (lightOn)
        {
            if (currLightEnergy > 0)
            {
                SetLightDirection();
                GetDamage();
            }
            else
            {
                lightOn = false;
            }
        }
        else
        {
            currLightEnergy += lightBoost * Time.deltaTime;
            if (currLightEnergy >= maxLightEnergy)
                currLightEnergy = maxLightEnergy;
        }

        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0)
        {
            lightOn = !lightOn;
            flashlightOnSound.Play();
        }

        lightPoint.SetActive(lightOn);

    }
}