﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    AudioSource music;
    public AudioClip secondTrack;
    float musicVol;

    private void Awake()
    {
        music = GetComponent<AudioSource>();
        musicVol = music.volume;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(musicTransition());
        }
    }

    IEnumerator musicTransition()
    {
        while(music.volume > 0)
        {
            yield return null;
            music.volume -= Time.deltaTime * 0.04f;
        }
        music.clip = secondTrack;
        music.Play();
        while(music.volume < musicVol)
        {
            yield return null;
            music.volume += Time.deltaTime * 0.04f;
        }
        music.volume = musicVol;
        Destroy(this);
    }
}
