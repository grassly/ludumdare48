﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpawnPoints : MonoBehaviour
{
    public List<Transform> allPoints = new List<Transform>();
    public Queue<Transform> spawnPoints = new Queue<Transform>();

    void Start()
    {
        EnemySpawnPoint[] points = FindObjectsOfType<EnemySpawnPoint>();
        foreach (EnemySpawnPoint point in points)
        {
            allPoints.Add(point.transform);
        }

        SortPoints();
    }

    void Update()
    {
        
    }

    public void SortPoints()
    {
        var sortedPoints = allPoints.OrderByDescending((p) => p.position.y).ToList();
        foreach (Transform point in sortedPoints)
        {
            //Debug.Log("pint added");
            spawnPoints.Enqueue(point);
        }
    }


    public Transform GetSpawnPoint()
    {
        if (spawnPoints.Count > 0)
            return spawnPoints.Peek();
        else
            return null;
    }

    public void DequeueSpawnPoint()
    {
        spawnPoints.Dequeue();
    }
}
