﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyOnLayer
{
    public string tag;
    public int amount;

    //можно добавить рандомную генерацию количества в range в зависимости от врагов через switch case
}

