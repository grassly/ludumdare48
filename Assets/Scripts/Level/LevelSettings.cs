﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[CreateAssetMenu(fileName = "LevelSettings", menuName = "LevelSettings")]
public class LevelSettings : ScriptableObject
{
    public float height;
    public List<EnemyOnLayer> enemys = new List<EnemyOnLayer>();
    public Queue<string> enemySpawnQueue = new Queue<string>();

    public string GetTag()
    {
        if (enemySpawnQueue != null)
            return enemySpawnQueue.Peek();
        else
            return null;
    }

    public void DequeueTag()
    {
        enemySpawnQueue.Dequeue();
    }

    public void InIt()
    {
        List<string> myName = new List<string>();

        foreach (EnemyOnLayer item in enemys)
        {
            for (int i = 0; i < item.amount; i++)
            {
                myName.Add(item.tag);
            }
        }

        myName = myName.OrderBy(notSortedTags => Guid.NewGuid()).ToList();

        enemySpawnQueue = new Queue<string>(myName);
        //Debug.Log($"init count: {enemySpawnQueue.Count}");
    }
}
