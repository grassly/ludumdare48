﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounsSpawner : MonoBehaviour
{
    public float minDistance;
    public int maxCount;
    public float playerMinDistance;
    public float respawnDelay;

    public Vector2 size = new Vector2(1f, 1f);
    public Vector2 center = new Vector2(0, 0);

    public GameObject spawnObject;
    public Transform player;

    Transform[] spawnPoints;
    

    private void Awake()
    {
    }
    void Start()
    {
        spawnPoints = new Transform[maxCount];
        for (int i = 0; i < maxCount; i++)
        {
            
            Vector3 pos = (Vector3)RandomVector((Vector2)transform.position + ((size / 2) - size * center), size) + new Vector3(0, 0, transform.position.z);
            bool tooClose = false;
            foreach(Transform point in spawnPoints)
            {
                if (point != null && Vector3.Distance(point.transform.position, pos) < minDistance)
                {
                    tooClose = true;
                    break;
                }
            }
            if (tooClose) continue;
            GameObject spawnPoint = new GameObject();
            spawnPoint.transform.position = pos;
            spawnPoints[i] = spawnPoint.transform;
        }

        StartCoroutine(RespawnBonus());
    }

    
    void Update()
    {

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position + (Vector3)((size / 2) - size * center), size);
    }

    Vector2 RandomVector(Vector2 center, Vector2 size)
    {
        Vector2 vector = new Vector2(
                Random.Range(center.x - size.x / 2, center.x + size.x / 2),
                Random.Range(center.y - size.y / 2, center.y + size.y / 2));
        return vector;
    }

    IEnumerator RespawnBonus()
    {
        while (true)
        {
            foreach (Transform point in spawnPoints)
            {
                if (point != null && point.childCount == 0 && Vector3.Distance(point.transform.position, player.position) > playerMinDistance)
                {
                    Instantiate(spawnObject, point.transform.position, new Quaternion(), point);
                }
            }
            yield return new WaitForSeconds(respawnDelay);
        }
    }
}
