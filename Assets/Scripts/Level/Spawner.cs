﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> prefabs = new List<GameObject>();
    public List<LevelSettings> settingsFiles = new List<LevelSettings>();

    private int currDepthLayer;
    private LevelSettings currDepth;

    public SpawnPoints points;

    [HideInInspector] public string Tag { get { return settingsFiles[currDepthLayer].GetTag(); } }

    public Controller character;

    [SerializeField] private float outScreenDistance;

    void Start()
    {
        foreach (LevelSettings settings in settingsFiles)
        {
            settings.InIt();
        }

        StartCoroutine(WaitForStartSpawn());
        //StartCoroutine(IdentifyDepth());
    }

    void Update()
    {
        currDepth = settingsFiles[currDepthLayer];
        Debug.Log($"{currDepthLayer} :: {settingsFiles.Count}");
        if (currDepthLayer < settingsFiles.Count-1 && settingsFiles[currDepthLayer+1].height > character.transform.position.y)
        {
            currDepthLayer++;
        }
    }

    public IEnumerator WaitForStartSpawn()
    {
        yield return new WaitForSeconds(1);
        StartCoroutine(StartSpawn());
    }

    public IEnumerator StartSpawn()
    {
        yield return new WaitForSeconds(3);
        SpawnEnemy();
    }

    public IEnumerator IdentifyDepth()
    {

        yield return new WaitForSeconds(5);
        currDepthLayer++;
    }

    public void SpawnEnemy()
    {
        Transform point = points.GetSpawnPoint();
        string curTag = Tag;
        if (curTag != null)
        {
            if (point != null)
            {
                //Debug.Log(Vector3.Distance(character.transform.position, point.position));
                if (Vector3.Distance(character.transform.position, point.position) >= outScreenDistance)
                {
                    foreach (GameObject prefab in prefabs)
                    {
                        if (prefab.CompareTag(curTag))
                        {
                            Debug.Log($"{curTag} :: {prefab}");
                            //Debug.Log("spawn!");
                            //Debug.Log(Tag);
                            Instantiate(prefab, point.position, point.rotation);
                            points.DequeueSpawnPoint();
                            currDepth.DequeueTag();

                            break;
                        }
                    }
                }
            }
        }

        StartCoroutine(StartSpawn());
    }
}
