using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FromCatscene : MonoBehaviour
{
    public int buildIndex;
    void Start()
    {
        SceneManager.LoadScene(buildIndex);
    }
}
