﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public KeyCode pauseKey = KeyCode.Escape;
    public GameObject pausePanel;
    public int mainMenuBuildIndex;

    public bool isPause { get; private set; }
    void Start()
    {
        pausePanel.SetActive(isPause);
    }

    void Update()
    {
        if (Input.GetKeyDown(pauseKey))
        {
            SetPause(!isPause);
        }
    }

    public void SetPause(bool isPause)
    {
        this.isPause = isPause;
        pausePanel.SetActive(isPause);
        Time.timeScale = isPause ? 0 : 1;
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(mainMenuBuildIndex);
    }
}
