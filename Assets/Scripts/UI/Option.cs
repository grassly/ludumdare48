﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class Option : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Button button;
    public GameObject arrows;
    public Text text;

    public Color unactive;
    public Color active;
    public float transitionDuration = 0.2f;

    bool hover;

    public void OnPointerEnter(PointerEventData eventData)
    {
        hover = true;
        //text.color = active;
        arrows.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hover = false;
        //text.color = unactive;
        arrows.SetActive(false);
    }

    private void Awake()
    {
        button = GetComponent<Button>();
    }
    void Start()
    {
        text.color = unactive;
        arrows.SetActive(false);
    }
    
    void Update()
    {
        if (hover)
        {
            text.color = Color.Lerp(text.color, active, transitionDuration);
        }
        else
        {
            text.color = Color.Lerp(text.color, unactive, transitionDuration);
        }
    }

    
}
