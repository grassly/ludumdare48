﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundsVolumeSetting : MonoBehaviour
{
    public AudioMixer mixer;
    Slider slider;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        slider.onValueChanged.AddListener(ChangeVolume);
    }
    void Start()
    {
        if (mixer.GetFloat("Volume", out float volume))
        {
            slider.value = volume;
        }
        
    }

    public void ChangeVolume(float value)
    {
        mixer.SetFloat("Volume", value > -30 ? value : -80);
    }
    
    void Update()
    {
        
    }
}
