﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashlightBar : MonoBehaviour
{
    public LampController light;
    Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
    }

    void Update()
    {
        slider.value = light.currLightEnergy / light.maxLightEnergy;
    }
}
