﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LosePanel : MonoBehaviour
{
    public int gameBuildIndex = 1;
    public int mainMenuBuildIndex = 0;

    public void Retry()
    {
        SceneManager.LoadScene(gameBuildIndex);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenuBuildIndex);
    }
}
