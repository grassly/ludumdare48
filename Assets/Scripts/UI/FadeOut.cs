﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
    public float fadeTime = 2;
    float time;
    Image image;
    void Start()
    {
        time = fadeTime;
        image = GetComponent<Image>();
    }

    
    void Update()
    {
        time -= Time.deltaTime;
        image.color = new Color(0, 0, 0, time / fadeTime);
    }
}
