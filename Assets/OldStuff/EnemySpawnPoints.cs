﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemySpawnPoints : MonoBehaviour
{
    public int minAmount;
    public int maxAmount;

    public float distance;
    public Transform character;

    private List<Transform> allPoints = new List<Transform>();
    public Queue<Transform> spawnPoints = new Queue<Transform>();

    //[HideInInspector] public Transform currSpawnPoint;

    void Start()
    {
        EnemySpawnPoint[] points = FindObjectsOfType<EnemySpawnPoint>();
        foreach (EnemySpawnPoint point in points)
        {
            allPoints.Add(point.transform);
        }

        SortPoints();
    }

    void Update()
    {
        /*foreach (Transform point in allPoints)
        {
            if (Vector3.Distance(character.position, point.transform.position) >= distance
                && !spawnPoints.Contains(point.transform))
            {
                //point.gameObject.SetActive(true);
                spawnPoints.Add(point);
            }
            else if (Vector3.Distance(character.position, point.transform.position) < distance
                && spawnPoints.Contains(point.transform))
                spawnPoints.Remove(point);
        }/*

        /*foreach (Transform point in spawnPoints)
        {
            if (Vector3.Distance(character.position, point.position) < distance
                && spawnPoints.Contains(point))
            {
                //point.gameObject.SetActive(false);
                spawnPoints.Remove(point);
                break;
            }
        }*/
    }

    public void SortPoints()
    {
        var sortedPoints = allPoints.OrderByDescending((p) => p.position.y);
        Queue<Transform> sortedPointsObj = new Queue<Transform>(sortedPoints);
        foreach (Transform point in sortedPointsObj)
        {
            spawnPoints.Enqueue(sortedPointsObj.Dequeue());
        }
    }

    public Transform GetSpawnPoint()
    {
        return spawnPoints.Dequeue();
    }
}
