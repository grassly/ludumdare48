﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public List<GameObject> prefabs = new List<GameObject>();

    public EnemySpawnPoints enemySpawn;

    public List<LevelSettings> settings = new List<LevelSettings>();
    //private Queue<string> enemySpawnQueue = new Queue<string>(); //{ get { return settings[(int)currDepth].GetTag(); } }
    private string enemySpawnQueue => settings[settingsSet].GetTag();

    private float currTime;
    public float gameTime;

    private float spawnTimer;
    public float minSpawnTime;
    public float maxSpawnTime;

    private float testTime;
    public float minTestTime;
    public float maxTestTime;

    public float shallowTime;
    public float averageTime;
    public float bottomTime;

    private int settingsSet;

    private int currPrefab;

    void Start()
    {
        settingsSet = 0;
        currTime = 0;
        testTime = Random.Range(minTestTime, maxTestTime);
        spawnTimer = Random.Range(minSpawnTime, maxSpawnTime);
        StartCoroutine(StartSpawnEnemy(spawnTimer));

        foreach (LevelSettings settingSet in settings)
        {
            settingSet.InIt();
        }
    }

    void Update()
    {
        if (currTime <= gameTime)
            currTime += Time.deltaTime;

        if (currTime > testTime && currTime <= shallowTime)
            settingsSet = 0;
        else if (currTime <= averageTime)
            settingsSet = 1;
        else if (currTime <= bottomTime)
            settingsSet = 2;

        if (enemySpawnQueue == null)
        {
            Debug.Log("queue is empty stupido!");
        }
    }

    public IEnumerator StartSpawnEnemy(float spawnTimer)
    {
        yield return new WaitForSeconds(testTime);
        StartCoroutine(SpawnEnemyCoroutine(spawnTimer));
    }

    public IEnumerator SpawnEnemyCoroutine(float currSpawnTimer)
    {
        yield return new WaitForSeconds(currSpawnTimer);
        SpawnEnemy();
    }

    public void SpawnEnemy()
    {
        Debug.Log(enemySpawnQueue);

        if (enemySpawnQueue != null)
        {
            foreach (GameObject prefab in prefabs)
            {
                if (prefab.CompareTag(enemySpawnQueue))
                {
                    for (int i = 0; i < prefabs.Count; i++)
                    {
                        if (prefabs[i].CompareTag(enemySpawnQueue))
                            currPrefab = i;
                    }
                    Debug.Log("spawn!" + enemySpawnQueue + currTime);
                    Transform currSpawnPoint = enemySpawn.GetSpawnPoint();
                    GameObject newEnemy = Instantiate(prefabs[currPrefab], currSpawnPoint.position, currSpawnPoint.rotation);

                    break;
                }
            }
        }

        spawnTimer = Random.Range(minSpawnTime, maxSpawnTime);

        StartCoroutine(SpawnEnemyCoroutine(spawnTimer));
        Debug.Log("new cor");
    }
}

//начать балансировку разнотипных префабов врагов